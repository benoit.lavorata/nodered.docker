# nodered.docker

Adds some required library to nodered/node-red-docker:v8 in order to run specific npm libraries:
- [Puppeteer](https://github.com/GoogleChrome/puppeteer), which is an interface to control Chrome browser through Javascript in headless mode (it runs on a server without graphical environment).

Automatic build.
Author: Benoit L.